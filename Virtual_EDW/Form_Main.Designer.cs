﻿namespace Virtual_Data_Warehouse
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /// 
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.menuStripMainMenu = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openCoreDirectoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openVDWConfigurationDirectoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openInputDirectoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openTemplateDirectoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openOutputDirectoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.openVDWConfigurationSettingsFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openTEAMConfigurationSettingsFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.openTemplateCollectionFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveTemplateCollectionFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.saveConfigurationFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.displayEventLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkBoxGenerateInDatabase = new System.Windows.Forms.CheckBox();
            this.SQLGenerationGroupBox = new System.Windows.Forms.GroupBox();
            this.checkBoxSaveToFile = new System.Windows.Forms.CheckBox();
            this.checkBoxGenerateJsonSchema = new System.Windows.Forms.CheckBox();
            this.backgroundWorkerActivateMetadata = new System.ComponentModel.BackgroundWorker();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.richTextBoxInformationMain = new System.Windows.Forms.RichTextBox();
            this.button12 = new System.Windows.Forms.Button();
            this.tabPageSettings = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxMetadataPath = new System.Windows.Forms.TextBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.labelTemplatePath = new System.Windows.Forms.Label();
            this.textBoxTemplatePath = new System.Windows.Forms.TextBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.groupBoxConfigurationPaths = new System.Windows.Forms.GroupBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.textBoxTeamConnectionsPath = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.textBoxTeamConfigurationPath = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxEnvironments = new System.Windows.Forms.ComboBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.textBoxTeamEnvironmentsFilePath = new System.Windows.Forms.TextBox();
            this.labelTEAMConfigurationFile = new System.Windows.Forms.Label();
            this.groupBoxOutputOptions = new System.Windows.Forms.GroupBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.textBoxSchemaName = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.OutputPathLabel = new System.Windows.Forms.Label();
            this.textBoxOutputPath = new System.Windows.Forms.TextBox();
            this.tabPageHome = new System.Windows.Forms.TabPage();
            this.labelWelcome = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.tabControlMain = new System.Windows.Forms.TabControl();
            this.backgroundWorkerEventLog = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.toolTipVdw = new System.Windows.Forms.ToolTip(this.components);
            this.menuStripMainMenu.SuspendLayout();
            this.SQLGenerationGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox9.SuspendLayout();
            this.tabPageSettings.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.groupBoxConfigurationPaths.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.groupBoxOutputOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.tabPageHome.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.tabControlMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStripMainMenu
            // 
            this.menuStripMainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStripMainMenu.Location = new System.Drawing.Point(0, 0);
            this.menuStripMainMenu.Name = "menuStripMainMenu";
            this.menuStripMainMenu.Size = new System.Drawing.Size(1259, 24);
            this.menuStripMainMenu.TabIndex = 4;
            this.menuStripMainMenu.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openCoreDirectoryToolStripMenuItem,
            this.openVDWConfigurationDirectoryToolStripMenuItem,
            this.openInputDirectoryToolStripMenuItem,
            this.openTemplateDirectoryToolStripMenuItem,
            this.openOutputDirectoryToolStripMenuItem,
            this.toolStripSeparator1,
            this.openVDWConfigurationSettingsFileToolStripMenuItem,
            this.openTEAMConfigurationSettingsFileToolStripMenuItem,
            this.toolStripSeparator2,
            this.openTemplateCollectionFileToolStripMenuItem,
            this.saveTemplateCollectionFileToolStripMenuItem,
            this.toolStripSeparator4,
            this.saveConfigurationFileToolStripMenuItem,
            this.toolStripSeparator3,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openCoreDirectoryToolStripMenuItem
            // 
            this.openCoreDirectoryToolStripMenuItem.Image = global::Virtual_Data_Warehouse.Properties.Resources.OpenDirectoryIcon;
            this.openCoreDirectoryToolStripMenuItem.Name = "openCoreDirectoryToolStripMenuItem";
            this.openCoreDirectoryToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.openCoreDirectoryToolStripMenuItem.Text = "Open Core Directory";
            this.openCoreDirectoryToolStripMenuItem.Click += new System.EventHandler(this.openCoreDirectoryToolStripMenuItem_Click);
            // 
            // openVDWConfigurationDirectoryToolStripMenuItem
            // 
            this.openVDWConfigurationDirectoryToolStripMenuItem.Image = global::Virtual_Data_Warehouse.Properties.Resources.OpenDirectoryIcon;
            this.openVDWConfigurationDirectoryToolStripMenuItem.Name = "openVDWConfigurationDirectoryToolStripMenuItem";
            this.openVDWConfigurationDirectoryToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.openVDWConfigurationDirectoryToolStripMenuItem.Text = "Open Configuration Directory";
            this.openVDWConfigurationDirectoryToolStripMenuItem.Click += new System.EventHandler(this.openVDWConfigurationDirectoryToolStripMenuItem_Click);
            // 
            // openInputDirectoryToolStripMenuItem
            // 
            this.openInputDirectoryToolStripMenuItem.Image = global::Virtual_Data_Warehouse.Properties.Resources.OpenDirectoryIcon;
            this.openInputDirectoryToolStripMenuItem.Name = "openInputDirectoryToolStripMenuItem";
            this.openInputDirectoryToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.openInputDirectoryToolStripMenuItem.Text = "Open Metadata Directory";
            this.openInputDirectoryToolStripMenuItem.Click += new System.EventHandler(this.openInputDirectoryToolStripMenuItem_Click);
            // 
            // openTemplateDirectoryToolStripMenuItem
            // 
            this.openTemplateDirectoryToolStripMenuItem.Image = global::Virtual_Data_Warehouse.Properties.Resources.OpenDirectoryIcon;
            this.openTemplateDirectoryToolStripMenuItem.Name = "openTemplateDirectoryToolStripMenuItem";
            this.openTemplateDirectoryToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.openTemplateDirectoryToolStripMenuItem.Text = "Open Template Directory";
            this.openTemplateDirectoryToolStripMenuItem.Click += new System.EventHandler(this.OpenTemplateDirectoryToolStripMenuItem_Click);
            // 
            // openOutputDirectoryToolStripMenuItem
            // 
            this.openOutputDirectoryToolStripMenuItem.Image = global::Virtual_Data_Warehouse.Properties.Resources.OpenDirectoryIcon;
            this.openOutputDirectoryToolStripMenuItem.Name = "openOutputDirectoryToolStripMenuItem";
            this.openOutputDirectoryToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.openOutputDirectoryToolStripMenuItem.Text = "Open Output Directory";
            this.openOutputDirectoryToolStripMenuItem.Click += new System.EventHandler(this.openOutputDirectoryToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(231, 6);
            // 
            // openVDWConfigurationSettingsFileToolStripMenuItem
            // 
            this.openVDWConfigurationSettingsFileToolStripMenuItem.Image = global::Virtual_Data_Warehouse.Properties.Resources.OpenFileIcon;
            this.openVDWConfigurationSettingsFileToolStripMenuItem.Name = "openVDWConfigurationSettingsFileToolStripMenuItem";
            this.openVDWConfigurationSettingsFileToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.openVDWConfigurationSettingsFileToolStripMenuItem.Text = "Open VDW Configuration File";
            this.openVDWConfigurationSettingsFileToolStripMenuItem.Click += new System.EventHandler(this.openVDWConfigurationSettingsFileToolStripMenuItem_Click);
            // 
            // openTEAMConfigurationSettingsFileToolStripMenuItem
            // 
            this.openTEAMConfigurationSettingsFileToolStripMenuItem.Image = global::Virtual_Data_Warehouse.Properties.Resources.OpenFileIcon;
            this.openTEAMConfigurationSettingsFileToolStripMenuItem.Name = "openTEAMConfigurationSettingsFileToolStripMenuItem";
            this.openTEAMConfigurationSettingsFileToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.openTEAMConfigurationSettingsFileToolStripMenuItem.Text = "Open TEAM Environments File";
            this.openTEAMConfigurationSettingsFileToolStripMenuItem.Click += new System.EventHandler(this.openTEAMConfigurationSettingsFileToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(231, 6);
            // 
            // openTemplateCollectionFileToolStripMenuItem
            // 
            this.openTemplateCollectionFileToolStripMenuItem.Image = global::Virtual_Data_Warehouse.Properties.Resources.OpenFileIcon;
            this.openTemplateCollectionFileToolStripMenuItem.Name = "openTemplateCollectionFileToolStripMenuItem";
            this.openTemplateCollectionFileToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.openTemplateCollectionFileToolStripMenuItem.Text = "Load Template Collection File";
            this.openTemplateCollectionFileToolStripMenuItem.Click += new System.EventHandler(this.openTemplateCollectionFileToolStripMenuItem_Click);
            // 
            // saveTemplateCollectionFileToolStripMenuItem
            // 
            this.saveTemplateCollectionFileToolStripMenuItem.Image = global::Virtual_Data_Warehouse.Properties.Resources.SaveFile;
            this.saveTemplateCollectionFileToolStripMenuItem.Name = "saveTemplateCollectionFileToolStripMenuItem";
            this.saveTemplateCollectionFileToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.saveTemplateCollectionFileToolStripMenuItem.Text = "Save Template Collection Grid";
            this.saveTemplateCollectionFileToolStripMenuItem.Click += new System.EventHandler(this.SaveTemplateCollectionFileToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(231, 6);
            // 
            // saveConfigurationFileToolStripMenuItem
            // 
            this.saveConfigurationFileToolStripMenuItem.Image = global::Virtual_Data_Warehouse.Properties.Resources.SaveFile;
            this.saveConfigurationFileToolStripMenuItem.Name = "saveConfigurationFileToolStripMenuItem";
            this.saveConfigurationFileToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveConfigurationFileToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.saveConfigurationFileToolStripMenuItem.Text = "Save Settings";
            this.saveConfigurationFileToolStripMenuItem.Click += new System.EventHandler(this.saveConfigurationFileToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(231, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Image = global::Virtual_Data_Warehouse.Properties.Resources.ExitApplication;
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem1,
            this.displayEventLogToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // helpToolStripMenuItem1
            // 
            this.helpToolStripMenuItem1.Image = global::Virtual_Data_Warehouse.Properties.Resources.HelpIconSmall;
            this.helpToolStripMenuItem1.Name = "helpToolStripMenuItem1";
            this.helpToolStripMenuItem1.Size = new System.Drawing.Size(167, 22);
            this.helpToolStripMenuItem1.Text = "Help";
            this.helpToolStripMenuItem1.Click += new System.EventHandler(this.helpToolStripMenuItem1_Click);
            // 
            // displayEventLogToolStripMenuItem
            // 
            this.displayEventLogToolStripMenuItem.Image = global::Virtual_Data_Warehouse.Properties.Resources.log_file;
            this.displayEventLogToolStripMenuItem.Name = "displayEventLogToolStripMenuItem";
            this.displayEventLogToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.displayEventLogToolStripMenuItem.Text = "Display Event Log";
            this.displayEventLogToolStripMenuItem.Click += new System.EventHandler(this.displayEventLogToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Image = global::Virtual_Data_Warehouse.Properties.Resources.RavosLogo;
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // checkBoxGenerateInDatabase
            // 
            this.checkBoxGenerateInDatabase.AutoSize = true;
            this.checkBoxGenerateInDatabase.Location = new System.Drawing.Point(6, 20);
            this.checkBoxGenerateInDatabase.Name = "checkBoxGenerateInDatabase";
            this.checkBoxGenerateInDatabase.Size = new System.Drawing.Size(192, 17);
            this.checkBoxGenerateInDatabase.TabIndex = 7;
            this.checkBoxGenerateInDatabase.Text = "Generate in database (SQL Server)";
            this.checkBoxGenerateInDatabase.UseVisualStyleBackColor = true;
            this.checkBoxGenerateInDatabase.CheckedChanged += new System.EventHandler(this.checkBoxGenerateInDatabase_CheckedChanged);
            // 
            // SQLGenerationGroupBox
            // 
            this.SQLGenerationGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.SQLGenerationGroupBox.Controls.Add(this.checkBoxSaveToFile);
            this.SQLGenerationGroupBox.Controls.Add(this.checkBoxGenerateJsonSchema);
            this.SQLGenerationGroupBox.Controls.Add(this.checkBoxGenerateInDatabase);
            this.SQLGenerationGroupBox.Location = new System.Drawing.Point(16, 580);
            this.SQLGenerationGroupBox.Name = "SQLGenerationGroupBox";
            this.SQLGenerationGroupBox.Size = new System.Drawing.Size(200, 106);
            this.SQLGenerationGroupBox.TabIndex = 14;
            this.SQLGenerationGroupBox.TabStop = false;
            this.SQLGenerationGroupBox.Text = "SQL Generation Options";
            // 
            // checkBoxSaveToFile
            // 
            this.checkBoxSaveToFile.AutoSize = true;
            this.checkBoxSaveToFile.Checked = true;
            this.checkBoxSaveToFile.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSaveToFile.Location = new System.Drawing.Point(6, 66);
            this.checkBoxSaveToFile.Name = "checkBoxSaveToFile";
            this.checkBoxSaveToFile.Size = new System.Drawing.Size(165, 17);
            this.checkBoxSaveToFile.TabIndex = 9;
            this.checkBoxSaveToFile.Text = "Save generation output to file";
            this.checkBoxSaveToFile.UseVisualStyleBackColor = true;
            this.checkBoxSaveToFile.CheckedChanged += new System.EventHandler(this.checkBoxSaveToFile_CheckedChanged);
            // 
            // checkBoxGenerateJsonSchema
            // 
            this.checkBoxGenerateJsonSchema.AutoSize = true;
            this.checkBoxGenerateJsonSchema.Location = new System.Drawing.Point(6, 43);
            this.checkBoxGenerateJsonSchema.Name = "checkBoxGenerateJsonSchema";
            this.checkBoxGenerateJsonSchema.Size = new System.Drawing.Size(182, 17);
            this.checkBoxGenerateJsonSchema.TabIndex = 8;
            this.checkBoxGenerateJsonSchema.Text = "Generate Json metadata schema";
            this.checkBoxGenerateJsonSchema.UseVisualStyleBackColor = true;
            this.checkBoxGenerateJsonSchema.CheckedChanged += new System.EventHandler(this.checkBoxGenerateJsonSchema_CheckedChanged);
            // 
            // backgroundWorkerActivateMetadata
            // 
            this.backgroundWorkerActivateMetadata.WorkerReportsProgress = true;
            this.backgroundWorkerActivateMetadata.WorkerSupportsCancellation = true;
            this.backgroundWorkerActivateMetadata.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker_DoWorkMetadataActivation);
            this.backgroundWorkerActivateMetadata.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorkerActivateMetadata_ProgressChanged);
            this.backgroundWorkerActivateMetadata.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerActivateMetadata_RunWorkerCompleted);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = global::Virtual_Data_Warehouse.Properties.Resources.RavosLogo;
            this.pictureBox1.Location = new System.Drawing.Point(1134, 586);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(109, 100);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 15;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox9
            // 
            this.groupBox9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox9.Controls.Add(this.richTextBoxInformationMain);
            this.groupBox9.Location = new System.Drawing.Point(337, 580);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(791, 106);
            this.groupBox9.TabIndex = 12;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Information";
            // 
            // richTextBoxInformationMain
            // 
            this.richTextBoxInformationMain.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBoxInformationMain.BackColor = System.Drawing.SystemColors.Control;
            this.richTextBoxInformationMain.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBoxInformationMain.Location = new System.Drawing.Point(6, 17);
            this.richTextBoxInformationMain.Name = "richTextBoxInformationMain";
            this.richTextBoxInformationMain.Size = new System.Drawing.Size(779, 82);
            this.richTextBoxInformationMain.TabIndex = 29;
            this.richTextBoxInformationMain.Text = "";
            this.richTextBoxInformationMain.TextChanged += new System.EventHandler(this.richTextBoxInformationMain_TextChanged);
            // 
            // button12
            // 
            this.button12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button12.Location = new System.Drawing.Point(222, 586);
            this.button12.MinimumSize = new System.Drawing.Size(109, 40);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(109, 40);
            this.button12.TabIndex = 23;
            this.button12.Text = "Refresh Metadata";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.buttonRefreshMetadata_Click);
            // 
            // tabPageSettings
            // 
            this.tabPageSettings.Controls.Add(this.groupBox1);
            this.tabPageSettings.Controls.Add(this.groupBoxConfigurationPaths);
            this.tabPageSettings.Controls.Add(this.groupBoxOutputOptions);
            this.tabPageSettings.Location = new System.Drawing.Point(4, 22);
            this.tabPageSettings.Name = "tabPageSettings";
            this.tabPageSettings.Size = new System.Drawing.Size(1227, 504);
            this.tabPageSettings.TabIndex = 8;
            this.tabPageSettings.Text = "Settings";
            this.tabPageSettings.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.textBoxMetadataPath);
            this.groupBox1.Controls.Add(this.pictureBox4);
            this.groupBox1.Controls.Add(this.labelTemplatePath);
            this.groupBox1.Controls.Add(this.textBoxTemplatePath);
            this.groupBox1.Controls.Add(this.pictureBox6);
            this.groupBox1.Location = new System.Drawing.Point(615, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(603, 92);
            this.groupBox1.TabIndex = 117;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "VDW input options";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 26);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(123, 13);
            this.label10.TabIndex = 94;
            this.label10.Text = "Metadata directory (json)";
            // 
            // textBoxMetadataPath
            // 
            this.textBoxMetadataPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxMetadataPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.textBoxMetadataPath.Location = new System.Drawing.Point(137, 23);
            this.textBoxMetadataPath.Multiline = true;
            this.textBoxMetadataPath.Name = "textBoxMetadataPath";
            this.textBoxMetadataPath.Size = new System.Drawing.Size(435, 20);
            this.textBoxMetadataPath.TabIndex = 93;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox4.Image = global::Virtual_Data_Warehouse.Properties.Resources.OpenDirectoryIcon;
            this.pictureBox4.Location = new System.Drawing.Point(578, 23);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(19, 20);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 96;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // labelTemplatePath
            // 
            this.labelTemplatePath.AutoSize = true;
            this.labelTemplatePath.Location = new System.Drawing.Point(6, 52);
            this.labelTemplatePath.Name = "labelTemplatePath";
            this.labelTemplatePath.Size = new System.Drawing.Size(94, 13);
            this.labelTemplatePath.TabIndex = 98;
            this.labelTemplatePath.Text = "Template directory";
            // 
            // textBoxTemplatePath
            // 
            this.textBoxTemplatePath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTemplatePath.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.textBoxTemplatePath.Location = new System.Drawing.Point(137, 49);
            this.textBoxTemplatePath.Multiline = true;
            this.textBoxTemplatePath.Name = "textBoxTemplatePath";
            this.textBoxTemplatePath.Size = new System.Drawing.Size(435, 20);
            this.textBoxTemplatePath.TabIndex = 97;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox6.Image = global::Virtual_Data_Warehouse.Properties.Resources.OpenDirectoryIcon;
            this.pictureBox6.Location = new System.Drawing.Point(578, 49);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(19, 20);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 99;
            this.pictureBox6.TabStop = false;
            this.pictureBox6.Click += new System.EventHandler(this.PictureBoxUpdateTemplatePath_Click);
            // 
            // groupBoxConfigurationPaths
            // 
            this.groupBoxConfigurationPaths.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxConfigurationPaths.Controls.Add(this.pictureBox8);
            this.groupBoxConfigurationPaths.Controls.Add(this.textBoxTeamConnectionsPath);
            this.groupBoxConfigurationPaths.Controls.Add(this.label3);
            this.groupBoxConfigurationPaths.Controls.Add(this.pictureBox7);
            this.groupBoxConfigurationPaths.Controls.Add(this.textBoxTeamConfigurationPath);
            this.groupBoxConfigurationPaths.Controls.Add(this.label2);
            this.groupBoxConfigurationPaths.Controls.Add(this.label1);
            this.groupBoxConfigurationPaths.Controls.Add(this.comboBoxEnvironments);
            this.groupBoxConfigurationPaths.Controls.Add(this.pictureBox3);
            this.groupBoxConfigurationPaths.Controls.Add(this.textBoxTeamEnvironmentsFilePath);
            this.groupBoxConfigurationPaths.Controls.Add(this.labelTEAMConfigurationFile);
            this.groupBoxConfigurationPaths.Location = new System.Drawing.Point(3, 3);
            this.groupBoxConfigurationPaths.Name = "groupBoxConfigurationPaths";
            this.groupBoxConfigurationPaths.Size = new System.Drawing.Size(606, 182);
            this.groupBoxConfigurationPaths.TabIndex = 100;
            this.groupBoxConfigurationPaths.TabStop = false;
            this.groupBoxConfigurationPaths.Text = "Configuration paths";
            // 
            // pictureBox8
            // 
            this.pictureBox8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox8.Image = global::Virtual_Data_Warehouse.Properties.Resources.OpenDirectoryIcon;
            this.pictureBox8.Location = new System.Drawing.Point(579, 145);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(19, 20);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox8.TabIndex = 119;
            this.pictureBox8.TabStop = false;
            this.pictureBox8.Click += new System.EventHandler(this.pictureBoxOpenConnectionFile_Click);
            // 
            // textBoxTeamConnectionsPath
            // 
            this.textBoxTeamConnectionsPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTeamConnectionsPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.textBoxTeamConnectionsPath.Location = new System.Drawing.Point(137, 145);
            this.textBoxTeamConnectionsPath.Multiline = true;
            this.textBoxTeamConnectionsPath.Name = "textBoxTeamConnectionsPath";
            this.textBoxTeamConnectionsPath.Size = new System.Drawing.Size(436, 20);
            this.textBoxTeamConnectionsPath.TabIndex = 117;
            this.toolTipVdw.SetToolTip(this.textBoxTeamConnectionsPath, resources.GetString("textBoxTeamConnectionsPath.ToolTip"));
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 148);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(122, 13);
            this.label3.TabIndex = 118;
            this.label3.Text = "TEAM connections path";
            // 
            // pictureBox7
            // 
            this.pictureBox7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox7.Image = global::Virtual_Data_Warehouse.Properties.Resources.OpenDirectoryIcon;
            this.pictureBox7.Location = new System.Drawing.Point(579, 118);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(19, 20);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 116;
            this.pictureBox7.TabStop = false;
            this.pictureBox7.Click += new System.EventHandler(this.pictureOpenTeamConfigurationFile_Click);
            // 
            // textBoxTeamConfigurationPath
            // 
            this.textBoxTeamConfigurationPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTeamConfigurationPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.textBoxTeamConfigurationPath.Location = new System.Drawing.Point(137, 118);
            this.textBoxTeamConfigurationPath.Multiline = true;
            this.textBoxTeamConfigurationPath.Name = "textBoxTeamConfigurationPath";
            this.textBoxTeamConfigurationPath.Size = new System.Drawing.Size(436, 20);
            this.textBoxTeamConfigurationPath.TabIndex = 114;
            this.toolTipVdw.SetToolTip(this.textBoxTeamConfigurationPath, resources.GetString("textBoxTeamConfigurationPath.ToolTip"));
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 121);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(125, 13);
            this.label2.TabIndex = 115;
            this.label2.Text = "TEAM configuration path";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 13);
            this.label1.TabIndex = 113;
            this.label1.Text = "Active environment";
            // 
            // comboBoxEnvironments
            // 
            this.comboBoxEnvironments.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxEnvironments.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxEnvironments.FormattingEnabled = true;
            this.comboBoxEnvironments.Location = new System.Drawing.Point(137, 48);
            this.comboBoxEnvironments.Name = "comboBoxEnvironments";
            this.comboBoxEnvironments.Size = new System.Drawing.Size(436, 21);
            this.comboBoxEnvironments.TabIndex = 112;
            this.comboBoxEnvironments.SelectedIndexChanged += new System.EventHandler(this.comboBoxEnvironments_SelectedIndexChanged);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox3.Image = global::Virtual_Data_Warehouse.Properties.Resources.OpenDirectoryIcon;
            this.pictureBox3.Location = new System.Drawing.Point(579, 22);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(19, 20);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 95;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBoxOpenEnvironmentFile_Click);
            // 
            // textBoxTeamEnvironmentsFilePath
            // 
            this.textBoxTeamEnvironmentsFilePath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTeamEnvironmentsFilePath.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.textBoxTeamEnvironmentsFilePath.Location = new System.Drawing.Point(137, 22);
            this.textBoxTeamEnvironmentsFilePath.Multiline = true;
            this.textBoxTeamEnvironmentsFilePath.Name = "textBoxTeamEnvironmentsFilePath";
            this.textBoxTeamEnvironmentsFilePath.Size = new System.Drawing.Size(436, 20);
            this.textBoxTeamEnvironmentsFilePath.TabIndex = 83;
            this.toolTipVdw.SetToolTip(this.textBoxTeamEnvironmentsFilePath, resources.GetString("textBoxTeamEnvironmentsFilePath.ToolTip"));
            // 
            // labelTEAMConfigurationFile
            // 
            this.labelTEAMConfigurationFile.AutoSize = true;
            this.labelTEAMConfigurationFile.Location = new System.Drawing.Point(6, 26);
            this.labelTEAMConfigurationFile.Name = "labelTEAMConfigurationFile";
            this.labelTEAMConfigurationFile.Size = new System.Drawing.Size(119, 13);
            this.labelTEAMConfigurationFile.TabIndex = 84;
            this.labelTEAMConfigurationFile.Text = "TEAM environments file";
            // 
            // groupBoxOutputOptions
            // 
            this.groupBoxOutputOptions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxOutputOptions.Controls.Add(this.pictureBox5);
            this.groupBoxOutputOptions.Controls.Add(this.textBoxSchemaName);
            this.groupBoxOutputOptions.Controls.Add(this.label12);
            this.groupBoxOutputOptions.Controls.Add(this.OutputPathLabel);
            this.groupBoxOutputOptions.Controls.Add(this.textBoxOutputPath);
            this.groupBoxOutputOptions.Location = new System.Drawing.Point(615, 101);
            this.groupBoxOutputOptions.Name = "groupBoxOutputOptions";
            this.groupBoxOutputOptions.Size = new System.Drawing.Size(603, 84);
            this.groupBoxOutputOptions.TabIndex = 89;
            this.groupBoxOutputOptions.TabStop = false;
            this.groupBoxOutputOptions.Text = "VDW output options";
            // 
            // pictureBox5
            // 
            this.pictureBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox5.Image = global::Virtual_Data_Warehouse.Properties.Resources.OpenDirectoryIcon;
            this.pictureBox5.Location = new System.Drawing.Point(578, 49);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(19, 20);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 97;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Click += new System.EventHandler(this.pictureBox5_Click);
            // 
            // textBoxSchemaName
            // 
            this.textBoxSchemaName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxSchemaName.Location = new System.Drawing.Point(137, 23);
            this.textBoxSchemaName.Name = "textBoxSchemaName";
            this.textBoxSchemaName.Size = new System.Drawing.Size(435, 20);
            this.textBoxSchemaName.TabIndex = 91;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 26);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(102, 13);
            this.label12.TabIndex = 92;
            this.label12.Text = "VDW schema name";
            // 
            // OutputPathLabel
            // 
            this.OutputPathLabel.AutoSize = true;
            this.OutputPathLabel.Location = new System.Drawing.Point(6, 52);
            this.OutputPathLabel.Name = "OutputPathLabel";
            this.OutputPathLabel.Size = new System.Drawing.Size(116, 13);
            this.OutputPathLabel.TabIndex = 82;
            this.OutputPathLabel.Text = "Output (spool) directory";
            // 
            // textBoxOutputPath
            // 
            this.textBoxOutputPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxOutputPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.textBoxOutputPath.Location = new System.Drawing.Point(137, 49);
            this.textBoxOutputPath.Multiline = true;
            this.textBoxOutputPath.Name = "textBoxOutputPath";
            this.textBoxOutputPath.Size = new System.Drawing.Size(435, 20);
            this.textBoxOutputPath.TabIndex = 81;
            // 
            // tabPageHome
            // 
            this.tabPageHome.Controls.Add(this.labelWelcome);
            this.tabPageHome.Controls.Add(this.pictureBox2);
            this.tabPageHome.Location = new System.Drawing.Point(4, 22);
            this.tabPageHome.Name = "tabPageHome";
            this.tabPageHome.Size = new System.Drawing.Size(1227, 504);
            this.tabPageHome.TabIndex = 12;
            this.tabPageHome.Text = "Home";
            this.tabPageHome.UseVisualStyleBackColor = true;
            // 
            // labelWelcome
            // 
            this.labelWelcome.AutoSize = true;
            this.labelWelcome.Location = new System.Drawing.Point(203, 31);
            this.labelWelcome.Name = "labelWelcome";
            this.labelWelcome.Size = new System.Drawing.Size(198, 13);
            this.labelWelcome.TabIndex = 85;
            this.labelWelcome.Text = "Welcome to the Virtual Data Warehouse";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Virtual_Data_Warehouse.Properties.Resources.RavosLogo;
            this.pictureBox2.Location = new System.Drawing.Point(34, 31);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(160, 152);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 18;
            this.pictureBox2.TabStop = false;
            // 
            // tabControlMain
            // 
            this.tabControlMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControlMain.Controls.Add(this.tabPageHome);
            this.tabControlMain.Controls.Add(this.tabPageSettings);
            this.tabControlMain.Location = new System.Drawing.Point(12, 32);
            this.tabControlMain.Name = "tabControlMain";
            this.tabControlMain.SelectedIndex = 0;
            this.tabControlMain.Size = new System.Drawing.Size(1235, 530);
            this.tabControlMain.TabIndex = 3;
            this.tabControlMain.SelectedIndexChanged += new System.EventHandler(this.tabControlMain_SelectedIndexChanged);
            // 
            // backgroundWorkerEventLog
            // 
            this.backgroundWorkerEventLog.WorkerReportsProgress = true;
            this.backgroundWorkerEventLog.WorkerSupportsCancellation = true;
            this.backgroundWorkerEventLog.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerEventLog_DoWork);
            this.backgroundWorkerEventLog.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorkerEventLog_ProgressChanged);
            this.backgroundWorkerEventLog.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerEventLog_RunWorkerCompleted);
            // 
            // toolTipVdw
            // 
            this.toolTipVdw.AutoPopDelay = 5000;
            this.toolTipVdw.InitialDelay = 300;
            this.toolTipVdw.ReshowDelay = 100;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1259, 700);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.groupBox9);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.SQLGenerationGroupBox);
            this.Controls.Add(this.tabControlMain);
            this.Controls.Add(this.menuStripMainMenu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStripMainMenu;
            this.MinimumSize = new System.Drawing.Size(800, 600);
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Virtual Data Warehouse";
            this.menuStripMainMenu.ResumeLayout(false);
            this.menuStripMainMenu.PerformLayout();
            this.SQLGenerationGroupBox.ResumeLayout(false);
            this.SQLGenerationGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox9.ResumeLayout(false);
            this.tabPageSettings.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.groupBoxConfigurationPaths.ResumeLayout(false);
            this.groupBoxConfigurationPaths.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.groupBoxOutputOptions.ResumeLayout(false);
            this.groupBoxOutputOptions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.tabPageHome.ResumeLayout(false);
            this.tabPageHome.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.tabControlMain.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStripMainMenu;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openOutputDirectoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.CheckBox checkBoxGenerateInDatabase;
        private System.Windows.Forms.GroupBox SQLGenerationGroupBox;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem1;
        private System.Windows.Forms.PictureBox pictureBox1;
        public System.ComponentModel.BackgroundWorker backgroundWorkerActivateMetadata;
        private System.Windows.Forms.ToolStripMenuItem saveConfigurationFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem openTEAMConfigurationSettingsFileToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.RichTextBox richTextBoxInformationMain;
        private System.Windows.Forms.CheckBox checkBoxGenerateJsonSchema;
        private System.Windows.Forms.CheckBox checkBoxSaveToFile;
        private System.Windows.Forms.ToolStripMenuItem openVDWConfigurationSettingsFileToolStripMenuItem;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.TabPage tabPageSettings;
        internal System.Windows.Forms.TextBox textBoxTeamEnvironmentsFilePath;
        internal System.Windows.Forms.TextBox textBoxOutputPath;
        internal System.Windows.Forms.GroupBox groupBoxOutputOptions;
        internal System.Windows.Forms.TextBox textBoxSchemaName;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label labelTEAMConfigurationFile;
        private System.Windows.Forms.Label OutputPathLabel;
        private System.Windows.Forms.TabPage tabPageHome;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TabControl tabControlMain;
        private System.Windows.Forms.GroupBox groupBoxConfigurationPaths;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.ToolStripMenuItem openVDWConfigurationDirectoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openInputDirectoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem openTemplateCollectionFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveTemplateCollectionFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openTemplateDirectoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxEnvironments;
        private System.Windows.Forms.ToolStripMenuItem displayEventLogToolStripMenuItem;
        private System.ComponentModel.BackgroundWorker backgroundWorkerEventLog;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pictureBox7;
        internal System.Windows.Forms.TextBox textBoxTeamConfigurationPath;
        private System.Windows.Forms.Label label2;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Label label10;
        internal System.Windows.Forms.TextBox textBoxMetadataPath;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label labelTemplatePath;
        internal System.Windows.Forms.TextBox textBoxTemplatePath;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox8;
        internal System.Windows.Forms.TextBox textBoxTeamConnectionsPath;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolTip toolTipVdw;
        private System.Windows.Forms.Label labelWelcome;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openCoreDirectoryToolStripMenuItem;
    }
}

